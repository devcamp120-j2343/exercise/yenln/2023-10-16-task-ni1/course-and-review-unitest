const mongoose = require("mongoose");
const reviewModel = require("../models/review.model");
const courseModel = require("./../models/course.model");

const createReviewOfCourse = async (req, res) => {
    const {
        stars,
        note,
        courseId
    } = req.body;
   
    if (!note) {
       return res.status(400).json({
           message: "Note is required"
       })
    }

    if (isNaN(stars)) {
        return res.status(400).json({
            message: "Stars must be a number"
        })
    }

    try {
        const savedReview = await reviewModel.create({
            stars: stars,
            note: note
        })

        const savedCourse = await courseModel.findByIdAndUpdate(courseId, {
            $push: {
                reviews: savedReview._id
            }
        })

        if (!savedCourse) {
            return res.status(400).json({
                message: "Can not update course with provided Id : " + courseId
            })
        }

        return res.status(201).json(savedReview);   
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Unexpected error"
        })
    }
}

const getAllReviews = async (req, res) => {
    // B1: Thu thap du lieu
    // B2: Validate du lieu
    // B3: Xu ly du lieu
    try {
        const result = await reviewModel.find();

        return res.status(200).json({
            message: "Lay danh sach review thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getReviewById  = async (req, res) => {
    // B1: Thu thap du lieu
    const reviewId = req.params.reviewid;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(reviewId)) {
        return res.status(400).json({
            message: "Course ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await reviewModel.findById(reviewId);

        if(result) {
            return res.status(200).json({
                message: "Lay thong tin review thanh cong",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin review"
            })
        }
        
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const updateReviewById = async (req, res) => {
    // B1: Thu thap du lieu
    const reviewid = req.params.reviewid;
    const {
        stars,
        note
    } = req.body;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(reviewid)) {
        return res.status(400).json({
            message: "Review ID khong hop le"
        })
    }

    if (typeof note !== "string") {
        return res.status(400).json({
            message: "Note must be string"
        })
     }
 
     if (isNaN(stars)) {
         return res.status(400).json({
             message: "Stars must be a number"
         })
     }

    // B3: Xu ly du lieu
    try {
        var newUpdateReview = {};

        if(note) {
            newUpdateReview.note = note;
        }
        if(stars) {
            newUpdateReview.stars = stars;
        }

        const result = await reviewModel.findByIdAndUpdate(reviewid, newUpdateReview);

        if(result) {
            const latestResult = await reviewModel.findById(reviewid);

            return res.status(200).json({
                message: "Update thong tin review thanh cong",
                data: latestResult
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin reivew"
            })
        }

    
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}   

const deleteReviewById = async (req, res) => {
    // B1: Thu thap du lieu
    const reviewid = req.params.reviewid;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(reviewid)) {
        return res.status(400).json({
            message: "Review ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await reviewModel.findByIdAndRemove(reviewid);

        if(result) {
            return res.status(200).json({
                message: "Xoa thong tin Review thanh cong"
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin Review"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}


module.exports = {
    createReviewOfCourse,
    getAllReviews,
    getReviewById,
    updateReviewById,
    deleteReviewById
}