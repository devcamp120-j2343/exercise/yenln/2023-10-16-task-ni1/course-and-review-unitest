const express = require("express");

const router = express.Router();

const {
    getAllReviewMiddleware,
    createReviewMiddleware,
    getReviewByIDMiddleware,
    deleteReviewMiddleware,
    updateReviewMiddleware
} = require("../middlewares/review.middleware");
const { createReviewOfCourse, getAllReviews, getReviewById, updateReviewById, deleteReviewById } = require("../controllers/review.controller");

router.get("/", getAllReviewMiddleware, getAllReviews);

router.post("/", createReviewMiddleware, createReviewOfCourse)

router.get("/:reviewid", getReviewByIDMiddleware, getReviewById)

router.put("/:reviewid", updateReviewMiddleware, updateReviewById)

router.delete("/:reviewid", deleteReviewMiddleware, deleteReviewById)

module.exports = router;