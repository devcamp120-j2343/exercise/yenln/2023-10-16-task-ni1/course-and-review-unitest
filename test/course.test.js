const chai = require("chai");
const chaiHttp = require('chai-http');
const server = require('../index');

const should = chai.should();

chai.use(chaiHttp);

describe('Test CRUD Course Restful API', () => {
    describe("Test get all course", () => {
        it("Result API get all must be array", (done) => {
            chai.request(server)
                .get("/api/v1/courses")
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.data.should.be.a("array");

                    done();
                })
        })
    })
});